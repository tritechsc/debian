https://linuxconfig.org/how-to-use-apache-to-redirect-all-traffic-from-http-to-https

Add this to the end of the virtual host
Redirect permanent / https://127.0.0.1
